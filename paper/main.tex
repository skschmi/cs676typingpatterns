\documentclass[a4paper]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{subcaption}
\graphicspath{{figures/}}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{listings}
\usepackage[toc,page]{appendix}
\makeatletter
\newcommand{\verbatimfont}[1]{\renewcommand{\verbatim@font}{\ttfamily#1}}
\makeatother

\title{Identity Classification from Typing Patterns}

\author{Nozomu Okuda, Steven Schmidt, Lawrence Thatcher}

\date{\today}

\begin{document}
\maketitle

\begin{abstract}
Three machine learning algorithms are described which use key-logging data to predict the identity of users.  Initial results are presented of experiments done to show the accuracy of these algorithms.
\end{abstract}

%%%%%%%%%%%%%%
%%%%%%%%%%%%%%
%%%%%%%%%%%%%%
\section{Introduction}

Modern interaction with computers requires some form of input.  Of the various computer interactions possible today, typing letters continues to be an important one.  It is also common knowledge that individual humans have habits.  We were interested in developing a machine learning model that can differentiate users based on their typing patterns.  If successful, such a model could potentially be used in privacy and security applications.  For example, this model could be used to detect an imposter using another user’s credentials in some web service.  The model could also be used to identify otherwise anonymous users on some public web service.

Previous work on the subject was published by Chairunnanda et al. in 2011 (\emph{Privacy: Gone with the Typing! Identifying Web Users by Their Typing Patterns} in PASSAT and SocialCom 2011).  They developed a simple system that compared the time differences between key press patterns among Internet users.  Their method was able to consistently predict the correct user in five-fold cross-validation.  Taking inspiration from this, we try to identify an individual by using three slightly different models.  We also changed the problem setting:  whereas Chairunnanda et al. asked users to type single words, we have asked users to type paragraphs from documents.

\section{Methods}

\subsection{Data Collection}

Before developing methods for identifying users, we wrote software to collect key press information.  As the user presses a key, the ASCII key code and the timestamp are recorded in a file, with each event on its own line.  In addition, the software displays text to the user in a user interface, highlighting in black the letters the user typed correctly and highlighting in red the letters the user typed incorrectly.  The highlighting is updated in light of corrections the user may make, but all key presses are recorded.

\begin{figure}
	\centering
    \includegraphics[width=0.75\textwidth]{keydetectui.png}
    \caption{Screenshot of user interface for collecting typing data.  Text typed correctly is highlighted in black; text typed incorrectly is highlighted in red.}
\end{figure}

We collected typing data from several BYU students over specific paragraphs in various documents, collecting enough data to do specific tests on six individuals.  These documents include the Gettysburg Address, an excerpt from a scholarly article on decision trees, an excerpt from a religious sermon on obedience, a religious declaration on families, and an e-mail message.  This data collected consists of a list of ASCII-codes and the corresponding time stamp (specifying the time, with millisecond-accuracy, of the key-strike), in the form shown below:
\begin{verbatim}
<ASCII-code 1>   <time stamp 1>
<ASCII-code 2>   <time stamp 2>
<ASCII-code 3>   <time stamp 3>
...
<ASCII-code n>   <time stamp n>
<ASCII-code n+1>   <time stamp n+1>
...
\end{verbatim}
From this raw data, we extract pairs of ASCII-codes and the differences in timestamps between the two key presses.  Let us call a difference in timestamps the time interval between the two key presses.  Examples of ASCII-code pairs include many common words or sub-words in English such as t-h, o-f, or e-i.  Since whitespace and punctuation are included, combinations such as [space]-[letter], [comma]-[space], and [apostrophe]-[letter] are also ASCII-pairs in which we have interest.  With these ASCII-code pairs and time intervals, we create a dictionary, where the key is an ASCII-code pair, and the value is a collection of all time intervals associated with the given ASCII-code pair.  Below is another representation for expressing what we have described in this paragraph:

\begin{verbatim}
<time interval> = <time stamp n+1> - <time stamp n>
key = (<ASCII-code n>,<ASCII-code n+1>)
value = [<time interval 1>, <time interval 2>, ... , <time interval k>]
dictionary = {key1 : value1, key2 : value 2, ... , key_n : value_n}
\end{verbatim}

The dictionaries described above are created for each document typed by each user.  These are used in the algorithms explained below.

%%%%%%%%%%%%%%
%%%%%%%%%%%%%%
%%%%%%%%%%%%%%
\subsection{Algorithms}
\subsubsection{Fit to Log-Normal Distribution}

Let us assume that the time interval data for each ASCII-code pair is a sample from a probability density function (PDF), one which is unique to a user, and which remains consistent for each time that an ASCII-code pair is typed.  The x-axis of this PDF represents the time interval between the key-strikes, and the y-axis is the magnitude of the probability density for the given time interval.  For each ASCII-code pair $(c_1,c_2)$ encountered frequently enough, it is possible to compute an approximation for the PDF.

Initially, we used the naive assumption that the PDF was a Normal distribution
\[
N(x) = \frac{1}{\sigma(data) \sqrt{2 \pi}} \exp\left(-\frac{(x-\mu(data))^2}{2 \sigma(data)^2}\right)
\]
of which the standard deviation $\sigma(data)$ and the mean $\mu(data)$ are computed as functions of the time interval data for each ASCII-code pair.  This results in a PDF similar to the one shown in Figure~\ref{fig:plot_normal_ex}.

However, there are a couple problems with a simple Normal distribution.  Mainly, it is that the data does not extend into the negative-values, and the data is not symmetric (with large tails only in the large-value regime) both of which the simple Normal distribution assumes.  To solve these problems (with a few tips from a BYU statistics professor), instead we find a normal distribution that corresponds to the \emph{log} of the data.
\[
N_{\log}(x) = \frac{1}{\sigma(\log(data)) \sqrt{2 \pi}}
\exp
\left(
-\frac{(\log(x)-\mu(\log(data)))^2}{2 \sigma(\log(data))^2}
\right)
\]
where $\sigma(\log(data))$ is the standard deviation of the \emph{log} of the data values, and $\mu(\log(data))$ is the mean of the log of the data values.  The idea here is that we compute the same Normal distribution as before, except for on a new set of data points computed as the \emph{log} of the original data.  The equation above is used to compute this distribution in the original data space.  Figure~\ref{fig:plot_log_normal_ex} shows an example of the Log-Normal PDF distribution.  Notice how the curve fits the data much more closely by being non-zero in only the positive domain and by having an asymmetry that gives a fatter tail for larger time interval values.

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{plot_normal_ex.pdf} 
  \caption{}
  \label{fig:plot_normal_ex}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{plot_log_normal_ex.pdf} 
  \caption{}
  \label{fig:plot_log_normal_ex}
\end{subfigure}
\caption{Normal distribution PDF (left); Log-Normal distribution PDF (right); computed from a particular ASCII-code pair's time interval data.}
\label{fig:plots_log_normal_and_normal_ex}
\end{figure}

For each known identity, an identity profile is created by reading in all the key-logging data for that identity, generating the PDFs for each ASCII-code pair, and saving this information in an accessible format.  This allows the value of the PDF to be computed for each triple $(\text{Identity}, \text{ASCII-code pair}, \text{time interval})$.  Figure~\ref{fig:box_plot_example} shows an example of a few select ASCII-code pairs and their respective distributions for two different users.

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{boxplot_example1.pdf} 
  \caption{}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{boxplot_example2.pdf} 
  \caption{}
\end{subfigure}
\caption{An example of a few select ASCII-code pairs and their respective time interval distributions for two different users, displayed using box plots.  The difference between the distributions of the two users is apparent.}
\label{fig:box_plot_example}
\end{figure}


Once the ASCII-code pair data is assembled for each known identity, this algorithm can then predict the identity for an unknown user by measuring the time intervals of each detected ASCII-code pair and then computing the corresponding probability density for each identity.  The probability density $d^{i}_k$ can be written as  
\[
d^{i}_k = N_{\log}(t_k,\sigma_{i,(c_1,c_2)},\mu_{i,(c_1,c_2)})
\]
for identity $i$ and ASCII-code pair event $k$ (comprising of ASCII-code pair $(c_1,c_2)$ and time interval $t_k$).
As each new ASCII-code pair is detected, a sum probability density is maintained for each possible identity.  The sum of the probability densities for each identity $i$ is computed as
\[
d^{i}_{\text{sum}} = \sum_k d^{i}_k \;.
\]
The identity that has the maximum sum probability-density is the current predicted identity of the user.
\[
\text{Predicted Identity} =  \text{max}_i(d^{i}_{\text{sum}})
\]
This algorithm saves the probability density values for the 200 most recent ASCII-code pair events, for each identity, and uses this data to make the prediction.







\subsubsection{Using Kernel Density Estimates}

Instead of fitting the data to a log-normal distribution, we can also fit the data (not the \emph{log} of the data) to an arbitrary distribution built from a kernel density estimate (abbreviated as kde).  While we defer details on this technique to reputable sources outside of this paper (the code makes use of scikit-learn's sklearn.neighbors.KernelDensity), the basic intuition behind the kernel density estimate is to sum together multiple pre-defined distributions called kernels, with each kernel centered where each data point is located.  In this case, the time intervals for a given ASCII-code pair are the data points.  Using these estimated distributions, we can compute a density for a given time interval for a given ASCII-code pair.  By aggregating these densities per identity, the algorithm votes for the identity with the highest aggregated density.  In addition, these density aggregates are computed based on the last 200 keys pressed.  Thus, this algorithm is very similar to the above described fit to log-normal distribution strategy, except that instead of fitting log-normal distributions to the data, this algorithm uses a kernel density estimate to compute densities for aggregation.


\subsubsection{Using Hidden Markov Models}

We also took a substantially different approach where rather than looking at distributions of time intervals for ASCII-code pairs, we tried to model the problem as a series of state transitions. For this approach we used a standard Hidden Markov Model (HMM) python library: HmmLearn. While we also defer the details of this technique to external sources, our basic approach was to gather all the training data for each user and fit a unique HMM with Gaussian emissions to their data. During this step the HMM would try to automatically infer a set of hidden states (in our case: three) and calculate the transition probabilities between them from the individual's data. For testing we let each model score the log probability of the given data matching the model, and then poll each person's model to vote on whichever model had the highest score. Similar to above, we also only considered the last 200 keys pressed in our time window, thus allowing for the same interactivity as the other two approaches.
\subsection{Experiments}


In order to evaluate the effectiveness of our algorithms, we tried three experiments.

Our first experiment was a three-fold cross-validation using Gettysburg Address data from four individuals.  All four individuals typed the Gettysburg Address three times, so the cross validation trained on two typing sessions and tested on a third (of course, this process was repeated so that each session was a test instance).  Accuracy is measured based on the final identity predicted by each of the models.

Our second experiment was a three-fold cross-validation using Gettysburg Address data from six individuals.  While similar to the first experiment, this second experiment sees how well the algorithms perform when given more individuals to consider.

Our third experiment examines the accuracy of our algorithms when applied to test data that is different than that used for the training.  As training data, all three instances of the Gettysburg Address data was used for each of the six individuals.  Then, all six individuals also typed a text about obedience, which was used for the testing data.

\section{Results}

While we present our results in this section, the raw output is available in the Appendices.

\begin{table}
\centering
\begin{tabular}{c c c c}
 & log-norm & kde & hmm \\
lawrence & 100\% & 100\% & 100\% \\
nozomu & 100\% & 0\% & 33\% \\
steven & 100\% & 67\% & 67\% \\
wilson & 100\% & 100\% & 100\%
\end{tabular}
\caption{Results from experiment 1.  Along the left are the identities; along the top are the algorithms.  The results are rounded to the nearest percent.}
\label{tab:exp1}
\end{table}

The results for the first experiment are shown in Table~\ref{tab:exp1}.  As can be seen, the log-normal fitting algorithm performs best.  In addition, all of the algorithms were 100\% accurate in the case of lawrence and wilson.  However, both kde and hmm had problems.  For kde, it guessed steven once and wilson twice for nozomu, and it guessed wilson once for steven.  For hmm, it guessed steven once and wilson once for nozomu, and it guessed nozomu once for steven.  Since kde and hmm are not in agreement in their errors, it seems that fitting to the log of the data, as the log-normal algorithm does, is very effective.

\begin{table}
\centering
\begin{tabular}{c c c c}
 & log-norm & kde & hmm \\
jeff & 100\% & 67\% & 33\% \\
joseph & 0\% & 33\% & 67\% \\
lawrence & 100\% & 100\% & 33\% \\
nozomu & 67\% & 0\% & 0\% \\
steven & 100\% & 67\% & 67\% \\
wilson & 100\% & 100\% & 100\%
\end{tabular}
\caption{Results from experiment 2.  Along the left are the identities; along the top are the algorithms.  The results are rounded to the nearest percent.}
\label{tab:exp2}
\end{table}

The results for the second experiment are shown in Table~\ref{tab:exp2}.  With an additional two identities, it becomes a little bit more difficult for the algorithms to differentiate between the identities' signatures.  The log-norm method still does the best, but fails in a few instances, and fails completely to correctly identify joseph.  The hmm method does the worst of the three algorithms.  Interestingly, both kde and hmm never identify nozomu correctly while log-norm is not perfect in identifying nozomu.  Noting that both kde and hmm also fared poorly in experiment 1 with respect to identifying nozomu, the data from nozomu must be inherently difficult to identify in some respect.

\begin{table}
\centering
\begin{tabular}{c c c c}
 & log-norm & kde & hmm \\
jeff & 100\% & 0\% & 0\% \\
joseph & 0\% & 100\% & 100\% \\
lawrence & 100\% & 100\% & 100\% \\
nozomu & 100\% & 0\% & 0\% \\
steven & 100\% & 0\% & 0\% \\
wilson & 100\% & 100\% & 0\%
\end{tabular}
\caption{Results from experiment 3.  Along the left are the identities; along the top are the algorithms.  The results are rounded to the nearest percent.}
\label{tab:exp3}
\end{table}

The results of the third experiment are shown in Table~\ref{tab:exp3}.  The results from the third experiment are really only from a single test for each user-algorithm pair, since we only had one instance of the obedience text from each identity.  Again, the log-norm algorithm performed the best, but again failed to identify joseph.  Interestingly, both kde and hmm were able to identify joseph correctly in this task.  Nevertheless, kde and hmm again score worse, only identifying 3/6 and 2/6 (respectively) of the identities correctly.  

\section{Conclusion}

We have implemented three different ways of identifying users based on their typing habits.  The first uses a normal distribution of the log of the time interval data.  The second uses the kernel density estimate of the time interval data and the third uses an HMM of the time interval data.  Our results are mixed, but are successful enough to indicate that with additional work there could be additional accuracy.

First, any additional work should rely on a much larger training data set.  This paper's results only rely on a small handful of individuals typing a few paragraphs of text.  If we had data from hundreds of individuals and many days or months worth of key logging data, a better picture as to the potential accuracy and applicability of these methods could be seen.

We also believe it would be worth attempting to identify the aspects of typing that have the greatest ``signal'', and design an algorithm that only looks at those aspects of the typing, thus removing some of the ``noise''.  Perhaps this could be implemented by reducing the set of ASCII-code pairs considered during analysis to only those which best differentiate the identities in the data.

Finally, our results suggest that using the log of the time intervals is advantageous.  This approach may improve kde and hmm.


\begin{appendices}
\section{Results Output}
The following is the raw output of the test scripts which we ran to get the results.

\subsection{Experiment 1}
\verbatimfont{\small}
\begin{verbatim}
Input index used for testing:  0  (others used for training)
Guess for 'lawrence': {'log-norm': 'lawrence', 'typeroracle': 'lawrence', 'hhmoracle': 'lawrence'}
Guess for 'nozomu': {'log-norm': 'nozomu', 'typeroracle': 'steven', 'hhmoracle': 'steven'}
Guess for 'steven': {'log-norm': 'steven', 'typeroracle': 'steven', 'hhmoracle': 'steven'}
Guess for 'wilson': {'log-norm': 'wilson', 'typeroracle': 'wilson', 'hhmoracle': 'wilson'}
Input index used for testing:  1  (others used for training)
Guess for 'lawrence': {'log-norm': 'lawrence', 'typeroracle': 'lawrence', 'hhmoracle': 'lawrence'}
Guess for 'nozomu': {'log-norm': 'nozomu', 'typeroracle': 'wilson', 'hhmoracle': 'nozomu'}
Guess for 'steven': {'log-norm': 'steven', 'typeroracle': 'steven', 'hhmoracle': 'steven'}
Guess for 'wilson': {'log-norm': 'wilson', 'typeroracle': 'wilson', 'hhmoracle': 'wilson'}
Input index used for testing:  2  (others used for training)
Guess for 'lawrence': {'log-norm': 'lawrence', 'typeroracle': 'lawrence', 'hhmoracle': 'lawrence'}
Guess for 'nozomu': {'log-norm': 'nozomu', 'typeroracle': 'wilson', 'hhmoracle': 'wilson'}
Guess for 'steven': {'log-norm': 'steven', 'typeroracle': 'wilson', 'hhmoracle': 'nozomu'}
Guess for 'wilson': {'log-norm': 'wilson', 'typeroracle': 'wilson', 'hhmoracle': 'wilson'}
\end{verbatim}

\subsection{Experiment 2}
\begin{verbatim}
Cross-validation with same text:
File:  gettysburg
Input index used for testing:  0  (others used for training)
Guess for 'jeff': {'log-norm': 'jeff', 'typeroracle': 'jeff', 'hhmoracle': 'joseph'}
Guess for 'joseph': {'log-norm': 'jeff', 'typeroracle': 'joseph', 'hhmoracle': 'joseph'}
Guess for 'lawrence': {'log-norm': 'lawrence', 'typeroracle': 'lawrence', 'hhmoracle': 'joseph'}
Guess for 'nozomu': {'log-norm': 'nozomu', 'typeroracle': 'wilson', 'hhmoracle': 'joseph'}
Guess for 'wilson': {'log-norm': 'wilson', 'typeroracle': 'wilson', 'hhmoracle': 'wilson'}
Guess for 'steven': {'log-norm': 'steven', 'typeroracle': 'steven', 'hhmoracle': 'steven'}
Input index used for testing:  1  (others used for training)
Guess for 'jeff': {'log-norm': 'jeff', 'typeroracle': 'steven', 'hhmoracle': 'jeff'}
Guess for 'joseph': {'log-norm': 'jeff', 'typeroracle': 'joseph', 'hhmoracle': 'nozomu'}
Guess for 'lawrence': {'log-norm': 'lawrence', 'typeroracle': 'lawrence', 'hhmoracle': 'lawrence'}
Guess for 'nozomu': {'log-norm': 'nozomu', 'typeroracle': 'wilson', 'hhmoracle': 'jeff'}
Guess for 'steven': {'log-norm': 'steven', 'typeroracle': 'steven', 'hhmoracle': 'steven'}
Guess for 'wilson': {'log-norm': 'wilson', 'typeroracle': 'wilson', 'hhmoracle': 'wilson'}
Input index used for testing:  2  (others used for training)
Guess for 'jeff': {'log-norm': 'jeff', 'typeroracle': 'jeff', 'hhmoracle': 'joseph'}
Guess for 'joseph': {'log-norm': 'jeff', 'typeroracle': 'wilson', 'hhmoracle': 'joseph'}
Guess for 'lawrence': {'log-norm': 'lawrence', 'typeroracle': 'lawrence', 'hhmoracle': 'joseph'}
Guess for 'nozomu': {'log-norm': 'jeff', 'typeroracle': 'wilson', 'hhmoracle': 'wilson'}
Guess for 'steven': {'log-norm': 'steven', 'typeroracle': 'wilson', 'hhmoracle': 'nozomu'}
Guess for 'wilson': {'log-norm': 'wilson', 'typeroracle': 'wilson', 'hhmoracle': 'wilson'}
\end{verbatim}

\subsection{Experiment 3}
\begin{verbatim}
Learn on Text A, test on Text B:
Learning Files:  gettysburg
Testing File:  obedience
Guess for 'joseph': {'log-norm': 'jeff', 'typeroracle': 'joseph', 'hhmoracle': 'joseph'}
Guess for 'steven': {'log-norm': 'steven', 'typeroracle': 'wilson', 'hhmoracle': 'nozomu'}
Guess for 'nozomu': {'log-norm': 'nozomu', 'typeroracle': 'wilson', 'hhmoracle': 'jeff'}
Guess for 'wilson': {'log-norm': 'wilson', 'typeroracle': 'wilson', 'hhmoracle': 'steven'}
Guess for 'lawrence': {'log-norm': 'lawrence', 'typeroracle': 'lawrence', 'hhmoracle': 'lawrence'}
Guess for 'jeff': {'log-norm': 'jeff', 'typeroracle': 'wilson', 'hhmoracle': 'nozomu'}
\end{verbatim}

\section{Link to Git Repository of Algorithm Implementation}

The Python 3 code which was written and used to generate the results described in this paper may be accessed by cloning the git repository at the following URL.

\begin{verbatim}
https://skschmi@bitbucket.org/skschmi/cs676typingpatterns.git
\end{verbatim}

\end{appendices}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}



